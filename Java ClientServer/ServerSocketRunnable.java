/* ******************************************************************************
 * File Name: ServerSocketRunnable
 * Author: Brodie Taillefer
 * Course: CST 8221- JAP, Lab Section:012
 * Assignment: 2 part 2
 * Date: April 12, 2015
 * Professor: Svillen Ranev
*  Purpose: Class ServerSocketRunnable implements Runnable, which means it is intended to be
*  			executed by a thread. This thread will grab the Client's input and parse the command
*  			sent, returning the response, the server.
***********************************************************************************/
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Class ServerSocketRunnable is intended to be executed by our thead, it will take the client input
 * parse the command and output the server response.
 * @version V1.0
 * @author Brodie
 * @see java.io.IOException
 * @see java.io.ObjectInputStream
 * @see java.io.ObjectOutputStream
 * @see java.net.Socket
 * @see java.text.DateFormat
 * @see java.text.SimpleDateFormat
 * @see java.util.Calendar
 * @see java.util.Date
 */
public class ServerSocketRunnable implements Runnable {
	/** The Client socket */
	private Socket socket;
	/** The input string from the client */
	String input;
	/** The output string the server sends to the client */
	String output;
	/**
	 * Default constructor, sets the instance variable socket to the parameter
	 * @param socket
	 */
	public ServerSocketRunnable(Socket socket) {
		this.socket = socket;
	}
	@Override
	/**
	 * Overridden method, run(). Is started when the thread is created.
	 */
	public void run() {
		ObjectOutputStream out; //Output stream
		ObjectInputStream in; //Input stream
		try {
			 out = new ObjectOutputStream(socket.getOutputStream()); //Try to get the socket's output stream
			 in = new ObjectInputStream(socket.getInputStream());	 //Try to get the socket's input stream
		} catch (IOException e) { //Catch if we couldnt create the streams
			System.out.printf("Couldn't create output/input Stream\n");
			return;
		}
		while(true) { //While we still want to get responses from the server
			String command = null; //The final command we will parse
			try { //Try to get the Client's request
				input = (String) in.readObject();
			} catch (ClassNotFoundException | IOException e1) {
				return;
			}
			int index = input.indexOf('>'); //See what index the > is located at
			if(index == -1 || input.startsWith(">")) { //If we have an invalid command
				output = "ERROR: Unrecognized command.";
			}
			else { //Else let's take the new command with the > cut off
				command = input.substring(0,index);
			}
				try { //Try to write to the output stream
					if(command.equals("END")) { //If the request is END
						out.writeObject("Connection closed"); //Write back that we wish to close the connection
						System.out.println("Server Socket: Closing client connection..."); 
						return;
					}
					else if(command.equals("TIME")) { //If the request is TIME
						DateFormat time = new SimpleDateFormat("hh:mm:ss a"); //Create a new DateFormat in the format of Hour:minutes AM/PM
						Calendar cal = Calendar.getInstance(); //Create a calender instance
						output = command + ": " + time.format(cal.getTime()); //Format our output
					}
					else if(command.equals("DATE")) { //If the request is DATE
						DateFormat date = new SimpleDateFormat("d MMMMM y"); //Create a new DateFormat in the correct format
						Date dates = new Date(); //Get the current date
						output = command +": " + date.format(dates); //Format our output
					}
					else if(command.equals("ECHO")) { //If the request is ECHO
						output = input.replaceFirst(">", ":"); //Take our original request and replace the first >
					}
					else if(command.equals("?")) { //If the request is ?, print the server commands
						output = "AVAILABLE SERVICES:\nEND\nECHO\nTIME\nDATE\n?\nCLS\n";
					}
					else if(command.equals("CLS")) { //If the request is CLS
						output = "CLS";
					}
					else { //Else it wasn't a command the server recognizes
						output = "ERROR: Unrecognized command.";
					}
					out.writeObject(output); //Write to the client
					Thread.sleep(100); //Cause the thread to sleep
			} catch (IOException | InterruptedException e) { //
				System.out.println("An unexpected error occurred.");
			}
			
		}			
	}
}
	

