/* ******************************************************************************
 * File Name: Client.java
 * Author: Brodie Taillefer
 * Course: CST 8221- JAP, Lab Section:012
 * Assignment: 2 part 2
 * Date: April 12, 2015
 * Professor: Svillen Ranev
*  Purpose: Main method for the Client. Set the ClientView in the JFrame, and display
*  			the ClientView.
***********************************************************************************/
import java.awt.Dimension;
import javax.swing.JFrame;

/**
 * Client class, to initialize and create a ClientView and place it in a frame
 * @author Brodie Taillefer
 * @version V1.0
 * @see java.awt.Dimension
 * @see javax.swing.JFrame
 * @since 1.8_025
 *
 */
public class Client {
	/**
	 * Main method to initialize a ClientView
	 * @param args
	 */
	public static void main(String args[]) {
		javax.swing.SwingUtilities.invokeLater(new Runnable()
		{
			/**
			 * Run is called when an object implementing interface is created.
			 * The Calculator GUI is created and it's location is set by the operating system, with a minimum dimension of 600 x 550
			 */
			public void run()
			{ 
	       	 	JFrame f = new JFrame("Brodie Taillefer's Client"); //Create a Frame for the Client to be displayed
	       	 	f.setLocationByPlatform(true);
	       	 	f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Set's what will happen when the user closes the frame
	       	 	f.setPreferredSize(new Dimension(600,550));
	       	 	f.setMinimumSize(new Dimension(600,550)); //Set minimum size of the Client window
	       	 	f.setContentPane(new ClientView());//Add the Client GUI to the content pane
	       	 	f.pack();
	       	 	f.setVisible(true);
	       }
		});
	}
}



