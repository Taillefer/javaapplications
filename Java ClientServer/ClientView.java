/* ******************************************************************************
 * File Name: ClientView.java
 * Author: Brodie Taillefer
 * Course: CST 8221- JAP, Lab Section:012
 * Assignment: 2 part 2
 * Date: April 12, 2015
 * Professor: Svillen Ranev
*  Purpose: Class to create the ClientView. Creates the GUI components and places them in the correct
*  			panels and orientation. Also includes the inner classes Connect and Send for the button
*  			action listeners.
***********************************************************************************/
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

/**
 * ClientView class to create the Client's view, includes the default constructor
 * to initialize the GUI components 
 * @author Brodie Taillefer
 * @version V1.0
 * @see java.awt.BorderLayout
 * @see java.awt.Color
 * @see java.awt.Dimension
 * @see java.awt.FlowLayout
 * @see java.awt.event.KeyEvent
 * @see java.io.ObjectInputStream
 * @see java.io.ObjectOutputStream
 * @see java.net.Socket
 * @see javax.swing.BorderFactory
 * @see javax.swing.Box
 * @see javax.swing.JButton
 * @see javax.swing.JComboBox
 * @see javax.swing.JLabel
 * @see javax.swing.JPanel
 * @see javax.swing.JScrollPane
 * @see javax.swing.JTextArea
 * @see javax.swing.JTextField
 * @see javax.swing.border.EmptyBorder
 * @see javax.swing.border.TitledBorder
 *
 */
public class ClientView extends JPanel {
	/** Temporary SerialVersion ID */
	private static final long serialVersionUID = 1L;
	/** JTextField to hold the server host name */
	private JTextField hostField = new JTextField(); //Host Field
	/** Connect button, used to connect to a server socket */
	private JButton connectBtn = new JButton("Connect"); //Connect Button
	/** Send button, used to send a command to the server */
	private JButton sendBtn = new JButton("Send");
	/** Holds the command to be sent to the server */
	private JTextField requestField = new JTextField(); //Server Request Field
	/** Combo box to hold the default port numbers */
	private JComboBox<Object> list; //Combo box
	/** String array to hold the port numbers for the combo box */
	String[] listItems = {"","8088", "65000", "65535" }; //Combo box default items
	/** Client Socket */
	private static Socket socket;
	/** Static string to hold display that the command is from the client {@value #CLIENT} */
	private static final String CLIENT = "CLIENT>";
	/** Static string to hold display that the command is from the server {@value #SERVER} */
	private static final String SERVER = "SERVER>";
	/** Input stream for the Client */
	private static ObjectInputStream input;
	/** Output stream for the Client */
	private static ObjectOutputStream output;
	/** JTextArea to hold the Client/Server responses */
	private JTextArea terminalText;
	
	/** Default constructor for the ClinetView, intializes and places the GUI components **/
	public ClientView() {
		JPanel connectionNorth = new JPanel(new FlowLayout(FlowLayout.LEFT)); 
		JPanel connectionSouth = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel connectionPanel = new JPanel(new BorderLayout());
		JPanel northPanel = new JPanel(new BorderLayout());
		JPanel requestPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel terminalPanel = new JPanel(new BorderLayout());
		JLabel host = new JLabel("Host:");
		host.setDisplayedMnemonic(KeyEvent.VK_H); //Set label mnemonic to H
		host.setLabelFor(hostField); //The labels mneumonic will be for the host textfield
		JLabel port = new JLabel("Port:");
		port.setDisplayedMnemonic(KeyEvent.VK_P); //Set label mnemonic to P
		connectionPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(255, 0, 0), 10), "SET CONNECTION")); //Create red titled border
		connectionNorth.add(host);
		connectionNorth.add(hostField);
		connectionPanel.add(connectionNorth,BorderLayout.NORTH);
		
		list = new JComboBox<Object>(listItems); //Instantiate combobox with default items
		list.setPreferredSize(new Dimension(100,22));
		list.setEditable(true);
		list.setBackground(Color.WHITE);
		port.setLabelFor(list); //The port label will be for the combobox
		connectionSouth.add(port); //Add label to panel
		connectionSouth.add(Box.createHorizontalStrut(8)); //Create spacing between label and combobox
		connectionSouth.add(list); //Add combobox
		connectionSouth.add(connectBtn); //Add button
		
		connectBtn.setBackground(Color.RED);
		connectBtn.setPreferredSize(list.getPreferredSize());
		connectBtn.setMnemonic(KeyEvent.VK_C);
		connectBtn.addActionListener(new Connect());
		connectionPanel.add(connectionSouth,BorderLayout.CENTER);
		hostField.setPreferredSize(new Dimension(495,22));
		hostField.setText("localhost");
		hostField.setFocusable(true);
		hostField.setCaretPosition(0); //Set the carat position to most left
		northPanel.add(connectionPanel, BorderLayout.NORTH);
		host.setPreferredSize(new Dimension(40,40));
		requestPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(0,0,0),10), "CLIENT REQUEST")); //Create black titled border
		requestPanel.add(requestField);
		requestField.setPreferredSize(new Dimension(450,22));
		requestField.setText("Type a server request line");
		sendBtn.setMnemonic(KeyEvent.VK_S);//Set Send button mnemonic
		sendBtn.setEnabled(false);
		sendBtn.setPreferredSize(new Dimension(85,22));
		sendBtn.addActionListener(new Send());
		requestPanel.add(sendBtn);
		northPanel.add(requestPanel,BorderLayout.CENTER);
		TitledBorder border = new TitledBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(0,0,255),10)),"TERMINAL",2,TitledBorder.CENTER); //Create blue titled border
		terminalPanel.setBorder(border);
		terminalText = new JTextArea();
		terminalText.setEditable(false);
		JScrollPane scrollPane = new JScrollPane(terminalText);
		terminalText.setPreferredSize(new Dimension(500,500));
		//scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS); //Let the JTextArea always have a vertical scroll bar
		terminalPanel.add(scrollPane);
		this.setLayout(new BorderLayout());
		this.setBorder(new EmptyBorder(3,3,3,3));
		this.add(northPanel,BorderLayout.NORTH);
		this.add(terminalPanel,BorderLayout.CENTER);
		this.setPreferredSize(new Dimension(600,550));
	}
	
	/**
	 * Inner class connect which implements ActionLisenter for the connect Button.
	 * On actionPerformed, it attempts to connect to the Server via the information given
	 * @author Brodie Taillefer
	 * @version V1.0
	 * @see java.net.UnknownHostException
	 * @see java.io.IOException
	 * @see java.net.InetSocketAddress
	 * @see java.awt.event.ActionEvent
	 * @see java.awt.event.ActionListener
	 * @since 1.8_025
	 *
	 */
	public class Connect implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			int port; //The port of the server
			try { //Try to parse the combobox selection as a integer
				port = Integer.valueOf((String) list.getSelectedItem());
			}catch(NumberFormatException e){ //Catch if we couldn't parse the string
				terminalText.append(CLIENT + "ERROR: Invalid Port\n");
				return;
			}
			try { //Try to connect to the server
				InetSocketAddress address; //Set the socket address
				String host = hostField.getText();
				address = new InetSocketAddress(host, port); //Intialize the socket address
				socket = new Socket();
				socket.setSoTimeout(300); //set the socket timeout to 300ms
				socket.connect(address,port); //Connect to the socket
				output = new ObjectOutputStream(socket.getOutputStream()); //Setup the output stream
				output.flush();
				input = new ObjectInputStream(socket.getInputStream()); //Setup the input stream
			} catch (UnknownHostException ex){ //Catch if we couldn't resolve the hostname
				terminalText.append(CLIENT + "ERROR: Unknown Host.\n");
				return;
			} catch (Exception ex){ //Catch if we couldnt connect to the server
				terminalText.append(CLIENT + "ERROR: Connection refused: server is not available. Check port or restart server.\n");
				return;
			}
			connectBtn.setEnabled(false);
			connectBtn.setBackground(Color.BLUE);
			sendBtn.setEnabled(true);
			terminalText.append("Connected to " + socket.toString()+ "\n");
		}
	}
	
	/**
	 * Inner class connect which implements ActionLisenter for the send Button.
	 * On actionPerformed, it attempts to connect to the Server via the information given
	 * @author Brodie Taillefer
	 * @version V1.0
	 * @since 1.8_025
	 *
	 */
	public class Send implements ActionListener {
		/** String for the response from the server */
		private String response;
		@Override
		public void actionPerformed(ActionEvent event) {
			try {
				output.writeObject(requestField.getText()); //Output the client request to the server 
				output.flush();
				requestField.setText(""); //Reset the client request field
			}catch(IOException e) { //Catch if we couldnt write to the server
				terminalText.append("Could not write to server\n");
				return;
			}
			try {
				response = (String) input.readObject(); //Get the response from the server
			} catch(IOException | ClassNotFoundException e){
				terminalText.append("Could not get response from server\n"); //If we couldn't get the response
				return;
			}
			if(response.equals("CLS")) { //If the server returned "CLS" we should clear the terminal
				terminalText.setText("");
				return;
			}
			else if(response.equals("Connection closed")) { //If the server returned "Connection closed" we should close the socket
				try {
					output.close(); //Close output stream
					input.close(); //Close input stream
					socket.close(); //Close the socket
					terminalText.append(SERVER + response + "\n"); //Append the response
					terminalText.append(CLIENT  + response + "\n");
					sendBtn.setEnabled(false); //Disable the send button
					connectBtn.setEnabled(true); //Enable toe connection button
					connectBtn.setBackground(Color.RED);
					return;
				} catch (IOException e) {
					terminalText.append("Could not close the connection\n");
				}		
			}
			terminalText.append(SERVER + response + "\n");			
		}
		
	}
}