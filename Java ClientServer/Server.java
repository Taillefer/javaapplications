/* ******************************************************************************
 * File Name: Server.java
 * Author: Brodie Taillefer
 * Course: CST 8221- JAP, Lab Section:012
 * Assignment: 2 part 2
 * Date: April 12, 2015
 * Professor: Svillen Ranev
*  Purpose: Creates the server portion of the Client/Server. Allows the user to choose at
*  			the command line what port to run the server on.
***********************************************************************************/
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Creates the server portion of the Client/Server. Allows the user to choose at
*  	the command line what port to run the server on.
 * @author Brodie Taillefer
 * @version V1.0
 * @see java.io.IOException
 * @see java.net.ServerSocket
 * @see java.net.Socket
 * @since 1.8_025
 */
public class Server {
	public static void main (String args[]) throws IOException {
		int port;//The server port
		if(args.length == 1) { //Check to see if a command line arguement was given
			try { //Try to parse the port
				port = Integer.valueOf(args[0]);
				System.out.printf("Using port: %d \n",port);
			}catch(NumberFormatException e) { //We couldn't set the port properly
				port = 8088;
				System.out.printf("%s is not a valid port.\n",args[0]);
				System.out.printf("Using default port: %d\n",port);
			}
		}
		else { //No argument, use the default port
			port = 8088;
			System.out.printf("Using default port: %d\n",port);
		}
		ServerSocket serverSocket = new ServerSocket(port); //Create the server socket
		while(true) { //Loop until we close the socket
			Socket clientSocket = serverSocket.accept(); //Accept the connection 
			System.out.printf("Connecting to a client: %s\n", clientSocket.toString());
			new Thread(new ServerSocketRunnable(clientSocket)).start(); //Create a new ServerSocketRunnable thread
		}
		
	}
}
