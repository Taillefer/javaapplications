/* ******************************************************************************
 * File Name: Calculator.java
 * Author: Brodie Taillefer
 * Course: CST 8221- JAP, Lab Section: TBD
 * Assignment: 1
 * Date: February 13, 2015
 * Professor: Svillen Ranev
*  Purpose: Main method for the Calculator. It sets the calculator to display in 
*  			center of the screen, creating a splashscreen object to display the splash for 
*  			5 seconds before the main calculator is shown.
 ****************************************************************************** */

import java.awt.Dimension;
import javax.swing.JFrame;

/**
 * Class Calculator() is the main method for the program. Here we create a new splash screen with a delay of 5 seconds
 * and use the SwingUtilities.invokeLater() to call run() to create our main calculator GUI through CalculatorView()
 * @author Brodie Taillefer
 * @version V1.0
 * @see java.awt.Dimension
 * @see javax.swing.JFrame
 * @since 1.8_025
 */

public class Calculator {
	/**
	 * Main method to initialize a CalculatorView and it's SplashScreen.
	 * @param args
	 */
	public static void main(String args[]) {
		CalculatorSplashScreen splash = new CalculatorSplashScreen(5000); //Create splash screen with 5 second delay
 	 	splash.showSplashWindow(); //show the splash window

		javax.swing.SwingUtilities.invokeLater(new Runnable()
		{
			/**
			 * Run is called when an object implementing interface is created. In our run, we create the splash screen object and set a delay of 5 seconds.
			 * The Calculator GUI is created and it's location is set by the operating system, with a minimum dimension of 330 x 400.
			 * 
			 */
			public void run()
			{ 
	       	 	JFrame f = new JFrame("Calculator"); //Create a Frame for the Calculator to be displayed
	       	 	f.setLocationByPlatform(true);
	       	 	f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Set's what will happen when the user closes the frame
	       	 	f.setMinimumSize(new Dimension(330,400)); //Set minimum size of the Calculator window
	       	 	f.add(new CalculatorView()); //Add the Calculator GUI to the frame
	       	 	f.pack();
	       	 	f.setVisible(true);
	       }
		});
	}
}


