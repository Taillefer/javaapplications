/* ******************************************************************************
 * File Name: CalculatorModel.java
 * Author: Brodie Taillefer
 * Course: CST 8221- JAP, Lab Section: 011
 * Assignment: 1 part 2
 * Date: March 5, 2015
 * Professor: Svillen Ranev
*  Purpose: Used to store and calculate the result of the 2 operands and operation, includes error state flag and precision for display formatting
 *******************************************************************************/
public class CalculatorModel {
	/** String to hold the calculator's current mode of operation */
	private String mode;
	/** Double value to hold the calculator's calculated result */
	private double result;
	/** Double value to hold the operand2 for the calculation */
	private double operand2;
	/** Double value to hold the operand1 for the calculation */
	private double operand1;
	/** String value to hold the operation of the calculation */
	private String operation;
	/** Boolean flag to keep track if the calculator is in an error state */
	private boolean errorState;
	/**String value to keep track of the precision that the calculator should display the result in */
	private String precision;

	/**
	 * Default constructor for CalculatorModel initializes the mode to float, the precision to .00 and clears the calculator for the first input
	 */
	public CalculatorModel() {
		mode = "Float";
		precision = ".00";
		result = 0.0;
		clear();
	}
	
	/**
	 * Method getMode(), used to return the Calculator's current mode, either Int or Float
	 * @return mode The current operating mode of the calculator
	 */
	public String getMode() {
		return mode;
	}
	
	/**
	 * Method setMode(), used to set the calculator's mode
	 * @param mode The current operating mode of the calculator
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}
	
	/**
	 * Method getPrecisionZero(), used to return what the default calculator text is in each mode, 0.0 for Float, 0 for Int
	 */
	public String getPrecisionZero() {
		if(mode.equals("Float")) { //If we are in Float mode
			return "0.0";
		}
		else { //else we are in Int
			return "0";
		}
	}
	
	/**
	 * Method setOperation(), used to set the calculator's operation for the next calculation
	 * @param operation The operation for the calculation
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}
	
	/** Method setOperand1() used to set the first operand for the calculator
	 * @param op1 The First operand for the calculation
	 */
	public void setOperand1(double op1) {
		this.operand1 = op1;
	}
	
	/**Method setOperand2() used to set the second operand for the calculator
	 * @param op2 The second operand for the calculation
	 */
	public void setOperand2(double op2) {
		this.operand2 = op2;
	}
	/**Method getOperand1() used to return operand1 
	 * @return operand1 The first operand for the calculation
	 */
	public double getOperand1() {
		return operand1;
	}
	
	/**Method performCalculations() used to calculate the value with operand1 operator operand2
	 */
	public void performCalculations() {
		try {
			if(operation.equals("*")) { //calculate the result if the operation is *
				result = operand1 * operand2;
			}
			else if(operation.equals("-")) { //calculate the result if the operation is -
				result = operand1 - operand2;
			}
			else if(operation.equals("+")) { //calculate the result if the operation is +
				result = operand1 + operand2;
			}
			else if(operation.equals("\u00F7")) { //calculate the result if the operation is /
				result = operand1 / operand2;
			}
		}
		catch (Exception e) { //We caused an error, either overflow or divided by 0
			errorState = true;
			result = 0.0;
			return;
		}
		if(!isValidResultValue()) { //No error was caused but is the result NAN or infinite? this is an error
			errorState = true; //We are currently in an errorstate
			result = 0.0; //Reset result
		}
	}
	/**
	 * Checks if the result is a valid result, if it is either NAN or infinite it is an invalid result
	 * @return true/false
	 */
	public boolean isValidResultValue() { 
		if(!Double.isNaN(result) && !Double.isInfinite(result)) {
			return true;
		}
		return false;
	}
	/**
	 * Returns the result of the calculation
	 * @return Formatted number with the desired precision OR Error string "--"
	 */
	public String getResult() {
		if(errorState) { //If we are in an errorstate we cannot display the result, must display --
			return "--";
		}
		if(mode == "Int") { //If we are in int mode, we should display the result as an int
			return String.format("%d", (int) result);
		}
		else { //Else we are in float mode, must pay attention to precision
			if(precision == (".0")) { //if the precision is .0, display 1 trailing number
				return String.format("%.1f", result);
			}
			if(precision == (".00")) { //if the precision is .00, display 2 trailing numbers
				return String.format("%.2f", result);
			}
			if(precision == ("Sci")) { //If the precision mode is Scientific, print as E with 6 trailing decimals
				return String.format("%.6E",result);
			}
		}
		return "--";
	}

	/**
	 * Method getErrorState(), used to return the current state of the calculator
	 * @return errorState The calculators current errorstate (True/false)
	 */
	public boolean getErrorState() {
		return errorState;
	}

	/**
	 * Method clear(), used to reset the CalculatorModel's instance variables
	 */
	public void clear() {
		operand1 = operand2 = result = 0.0; //Reset the operands and result
		operation = "";
		errorState = false;
	}

	/**
	 * Method setOperand1WithResult(), used to set the current result to operand1, used when wanting to do an operation on a result already calculated
	 */
	public void setOperand1WithResult() {
		double result = this.result; 
		clear(); //Reset the calculator's flags and result
		operand1 = result;
	}
	
	/**
	 * Method setErrorState(), used to set the CalculatorModel's errorState flag
	 * @param state Is the calculator in an error state
	 */
	public void setErrorState(boolean state) {
		this.errorState = state;
	}
	
	/**
	 * Method setPrecision, used to set the CalculatorModel's precision 
	 * @param precision The precision of the calculation , Sci, .0 , .00
	 */
	public void setPrecision(String precision) {
		this.precision = precision;
	}
}

