/* ******************************************************************************
 * File Name: CalculatorSplashScreen.java
 * Author: Brodie Taillefer
 * Course: CST 8221- JAP, Lab Section: TBD
 * Assignment: 1
 * Date: February 13, 2015
 * Professor: Svillen Ranev
*  Purpose: Used to display a splash screen before the main calculator GUI is shown
*           this class includes the method showSplashWindow() to paint the splash screen
*           for 5 seconds before the main GUI shows.
 *******************************************************************************/


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

/**
 * Used to paint and display the Calculator's SplashScreen for x seconds
 * @author Brodie Taillefer
 * @version V1.0
 * @see java.awt.BorderLayout
 * @see java.awt.Color
 * @see java.awt.Font
 * @see javax.swing.BorderFactory
 * @see javax.swing.ImageIcon
 * @see javax.swing.JFrame
 * @see javax.swing.JLabel
 * @see javax.swing.JPanel
 * @since 1.8_025
 */
public class CalculatorSplashScreen  {
	/** Duration for the splash screen to show */
	private int duration;
	
	/**
	 * Constructor for the CalculatorSplashScreen class. Takes the duration of the splashscreen to show
	 * as a parameter.
	 * @param duration Duration for the splashscreen to show
	 */
	public CalculatorSplashScreen(int duration) {
	    this.duration = duration; //Set the splashscreen duration
	 }
	
	/**
	 * Method used to initialize and paint the Splash window to the screen before the main calculator GUI
	 * is shown. The splash screen is centered in the screen and includes the author's student number and name.
	 */
	public void showSplashWindow(){
		long oldTime = System.currentTimeMillis(); //Get the time in which the splash screen was called in ms
		long currentTime = 0; //Current Time start at 0, so we alteast loop once in our progress bar
		JFrame frame = new JFrame(); //Frame for the splash window
		frame.setUndecorated(true);
		JPanel splash = new JPanel(new BorderLayout()); //JPanel to add components for the splash screen	
		frame.setLocationByPlatform(true); //Allow the operating system to chose the window location
		JLabel label = new JLabel(new ImageIcon("calculator.jpg")); //New Image for the splash screen
		JLabel demo = new JLabel("Brodie Taillefer\n 040 757 711", JLabel.CENTER); //Label for the splash screen
		demo.setFont(new Font(Font.SERIF, Font.BOLD, 25));
		demo.setOpaque(true);
		demo.setBackground(Color.BLACK); //Set the label's bg color to Black
		demo.setForeground(Color.WHITE); //Font color white
		JProgressBar progress = new JProgressBar(0,duration); //Create new progress bar that with progress bar value between 0 and duration(5000ms)
		progress.setString("Loading Calculator. Please wait�"); //Set the progress bar text
		progress.setVisible(true); 
		progress.setStringPainted(true); //Display the string
		progress.setForeground(Color.GREEN);
		progress.setBorder(BorderFactory.createLineBorder(Color.WHITE,9));
		splash.add(progress,BorderLayout.NORTH);
		splash.setBorder(BorderFactory.createLineBorder(Color.BLACK, 9));
		splash.add(label, BorderLayout.CENTER);
		splash.add(demo, BorderLayout.SOUTH);
	  
		frame.add(splash); //Add the splash to the main frame
		frame.setContentPane(splash); //Set the splash screen in the content frame's content pane
		frame.pack();
		frame.setVisible(true);
		while (currentTime < duration){ //Splash screen wait time, get the current time since start and compare to the desired splashscreen duration 
			currentTime = System.currentTimeMillis() - oldTime; //Get the time since the splash screen was launched
			progress.setValue((int)currentTime); //Set the progress bar to the time since launch
		}
		//destroy the window and release all resources
		frame.dispose(); 
	}
}
