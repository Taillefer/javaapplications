/* ******************************************************************************
 * File Name: CalculatorView.java
 * Author: Brodie Taillefer
 * Course: CST 8221- JAP, Lab Section: TBD
 * Assignment: 1
 * Date: February 13, 2015
 * Professor: Svillen Ranev
*  Purpose: Used to create and paint the Calculator and it's GUI. This includes adding the Action method's for 
*  			each of the Calculator's buttons.
*  Class list: CalculatorView, Controller
 *******************************************************************************/


import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.*;
import javax.swing.border.EmptyBorder;


/**
 * Used to create and paint the Calculator's GUI and button action handler's. The calculator is arranged so that resizing
 * the calculator will not alter the appearance.
 * @author Brodie Taillefer
 * @version V1.0
 * @see java.awt.event.KeyEvent
 * @see javax.swing.border.EmptyBorder
 * @since 1.8_025
 */
public class CalculatorView extends JPanel{
	/** serialVersion of the application {@value #serialVersionUID}*/
	private static final long serialVersionUID = 1L;
	/**textField for the calculator display */
	private JTextField display;
	/** Label for the error */
	private JLabel error;
	/** JButton for the calculator's . button. It will be initialized using our button factory method, createButton() */
	private JButton dotButton;
	/** CalculatorModel to handle the calculator's calculation's */
	private CalculatorModel model;
	
	/** Controller to handle action events for every button in the calculator, all on one controller */
	private Controller controller;
	/** Default font size of {@value #DEFAULT_FONT_SIZE} */
	private final int DEFAULT_FONT_SIZE = 20;
	/** Static String unicode representation for the division button {@value #DIVISION} **/
	private static final String DIVISION = "\u00F7";
	/** Static String unicode representation for the multiplication button {@value #MULTIPLY} **/
	private static final String MULTIPLY = "\u002A";
	/** Static String unicode representation for the Minus button  {@value #MINUS} **/
	private static final String MINUS = "\u002D";
	/** Static String unicode representation for the Plus/Minus(Sign) button {@value PLUSMINUS} **/
	private static final String PLUSMINUS = "\u00B1";
	/** Static String unicode representation for the addition(plus) button {@value PLUS}**/
	private static final String PLUS = "\u002B";
	/** Static String unicode representation for the Backspace button {@value BACKSPACE}**/
	private static final String BACKSPACE = "\u2190";
	/**String array for the button labels  */
	private final static String[] LABELS = new String[] {"7", "8", "9", DIVISION, "4", "5", "6", MULTIPLY,
		"1", "2", "3", MINUS, PLUSMINUS, "0", ".", PLUS};
	/**
	 * Method used to initialize and paint the Calculator GUI, and attach the button's to their Action Listener
	 */
	public CalculatorView(){
		display = new JTextField(); //Initialize textField
		this.dotButton = new JButton();
		controller = new Controller();
		this.setLayout(new BorderLayout()); //Our Main layout for our JPanel will be a borderlayout
		this.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color.BLACK));
		JPanel upperPanel = new JPanel(new BorderLayout()); //JPanel with borderlayout for the upper panel 
		JPanel displayPanel = new JPanel(); //JPanel with default flow layout for the display panel
		JPanel lowerPanel = new JPanel(); //JPanel with default flow layout for the lower panel
		error = new JLabel("F");
		error.setPreferredSize(new Dimension(25,25)); //Set the size for the error label 
		error.setHorizontalAlignment(JLabel.CENTER); //Set the mode text to the center of the label
		error.setOpaque(true); //Set Opaque so we can add a bg colour
		error.setBackground(Color.YELLOW);
		displayPanel.add(error); //Add the mode display to displayPanel

		display.setEditable(false);
		display.setPreferredSize(new Dimension(0,30)); //Set textField height to 30
		display.setColumns(16);
		display.setBackground(Color.WHITE);
		display.setHorizontalAlignment(JTextField.RIGHT);
		display.setText("0.0");
		displayPanel.setBackground(Color.WHITE);//set the display panel bg to white
		displayPanel.add(display); //Add the textField to the displayPanel
		
		JButton backspace = new JButton("\u2190"); //Backspace button for first row
		backspace.setPreferredSize(new Dimension(25,25)); //Set backspace's button pref size
		backspace.setBorder(BorderFactory.createLineBorder(Color.RED,1));
		backspace.setForeground(Color.RED);
		backspace.setMnemonic(KeyEvent.VK_B); //Set mnemonic for backspace ALT + B
		backspace.setToolTipText("ALT - B)");
		backspace.setOpaque(false);
		backspace.setContentAreaFilled(false); //Set's the button transparant
		backspace.addActionListener(controller);
		displayPanel.add(backspace);
		
		upperPanel.add(displayPanel,BorderLayout.NORTH); //Add our displaypanel to the north part of our upper panel
		JCheckBox mode = new JCheckBox(); //Checkbox for the Int mode
		mode.setBackground(Color.GREEN);
		mode.addActionListener(controller);
		mode.setText("Int");
		mode.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		ButtonGroup precisionGroup = new ButtonGroup(); //Button group to hold the precision Radio Buttons
		JRadioButton zeroPrecision = new JRadioButton(); //0.0 Precision button
		
		zeroPrecision.addActionListener(controller);
		zeroPrecision.setText(".0");
		zeroPrecision.setBackground(Color.YELLOW);
		precisionGroup.add(zeroPrecision); //Add the Radio button to the buttongroup
		
		JRadioButton zeroZeroPrecision = new JRadioButton(); //0.00 Precision Button
		zeroZeroPrecision.addActionListener(new Controller());
		zeroZeroPrecision.setText(".00");
		zeroZeroPrecision.setSelected(true);
		zeroZeroPrecision.setBackground(Color.YELLOW);
		precisionGroup.add(zeroZeroPrecision); //Add the Radio button to the buttongroup
		
		JRadioButton sciPrecision = new JRadioButton(); //sci Precision Button
		sciPrecision.addActionListener(controller);
		sciPrecision.setText("Sci");
		sciPrecision.setBackground(Color.YELLOW);
		zeroPrecision.setPreferredSize(sciPrecision.getPreferredSize()); //Set the size of the button to the largest radio button's size
		zeroZeroPrecision.setPreferredSize(sciPrecision.getPreferredSize()); 
		mode.setPreferredSize(sciPrecision.getPreferredSize()); 
		precisionGroup.add(sciPrecision); //Add the radio button to the button group
		
		Box modeBox = new Box(BoxLayout.X_AXIS);	//Box Layout in the X Axis to hold the int mode and precision
		modeBox.setBackground(Color.BLACK);
		modeBox.add(Box.createHorizontalStrut(12));//Create horizontal struct to line up to mode with the textfield
		modeBox.add(mode);
		modeBox.add(Box.createHorizontalStrut(19)); //Create horizontal struct for space between mode and Radio Buttons
		modeBox.add(zeroPrecision);modeBox.add(zeroZeroPrecision);modeBox.add(sciPrecision);
		lowerPanel.add(modeBox); //Add Box to the lowerPanel 
		lowerPanel.setBackground(Color.BLACK);
		upperPanel.add(lowerPanel,BorderLayout.CENTER); //Add lowerPanel to the Center of the upperPanel with a centered layout
		this.add(upperPanel,BorderLayout.NORTH); //Add the upperPanel to the north of the main layout
		
		JButton clearButton = createButton("C","C",Color.BLACK,Color.RED,controller); //Create Clear Button
		upperPanel.add(clearButton,BorderLayout.SOUTH);
		
		GridLayout grid = new GridLayout(4,1,5,5); //Create grid layout with the following dimensions
		JPanel calcButtons = new JPanel(grid); //JPanel with grid layout for the calculator's buttons
		calcButtons.setBorder(new EmptyBorder(2,2,2,2));
		
		for(String labels : LABELS) { //Loop through the array of button name's and create the corresponding button
			JButton button; //Create button variable
			if(labels == ".") {
				button = createButton(labels,labels,Color.BLACK,Color.BLUE,controller);
				this.dotButton = button;
			}
			else if(!labels.matches("[\u00F7\\\u002A\\\u002D\\\u002B]")){ //If the label doesn't match the regex, the buttons fg should be black
				button = createButton(labels,labels,Color.BLACK,Color.BLUE,controller);
			}
			else { //Else it should be Yellow
				button = createButton(labels,labels,Color.YELLOW,Color.BLUE,controller);
			}
			calcButtons.add(button); //Add the button to the JPanel with GridLayout
		}
		JButton equals = createButton("\u003D", "\u003D", Color.BLACK, Color.YELLOW, controller); //Create equals Button
		equals.setFont(equals.getFont().deriveFont(Font.BOLD));
		equals.setPreferredSize(clearButton.getPreferredSize()); //Set it's size to the same as Clear Button
		this.add(calcButtons,BorderLayout.CENTER); //Set the grid layout of buttons in the main Panels Center
		this.add(equals,BorderLayout.SOUTH); //Set the equals button to south in the main panel
		this.setVisible(true); //Show GUI
	}
	
	/** Method used to create a button with the given properties. This is used to create all the buttons situated
	 * in the Calculator's grid layout. The button is also given a ActionListener to keep track of when the user
	 * pressed and releases the button.
	 * @param text The text for the button label 
	 * @param ac The Action command string for the button label
	 * @param fg The foreground color of the button
	 * @param bg The background color of the button
	 * @param handler The Action Handler for the button
	 * @return JButton Return reference to the button
	 */
	private JButton createButton(String text, String ac, Color fg, Color bg, ActionListener handler){
		JButton button = new JButton(); //Create button with no properties set
		button.setText(text);
		button.setBackground(bg);
		button.setForeground(fg);
		if(ac != null) { //If a valid action command string has been passed in
			button.setActionCommand(ac);
		}
		Font defaultFont = button.getFont(); //Get the default font that is currently be used
		button.setFont(new Font(defaultFont.getFontName(),defaultFont.getStyle(),DEFAULT_FONT_SIZE)); //Set Font to the default font being used and 20 font size
		button.addActionListener(handler);	//Add the ActionListener to the button
		return button;	
	}

	/**
	 * Inner class to create an ActionListener for the Calculator's buttons. Calls the various methods declared in the Controller depending on which 
	 * button actionevent is called.
	 * @author Brodie Taillefer
	 * @version V1.0
	 * @see java.awt.event.ActionEvent
	 * @since 1.8_025
	 */
	 private class Controller implements ActionListener{
		/** Boolean flag to check if operand 1 is set */
		private boolean op1set; 
		/** Boolean flag to check if operand 2 is set */
		private boolean op2set;
		/** Boolean flag to check if a result is currently displayed */
		private boolean resultset;
		/** Boolean flag to check if the display is currently in a clear state */
		private boolean clearDisplay;
		/** Text to hold the current calculator's display */
		private String text;
		/**
		 * Default constructor for the Controller class, initializes all the instance variables (flags and text), and creates a new reference to the CalculatorModel
		 * to deal with the calculations
		 */
		public Controller() {
			op1set = false; //Operation 1 is not set
			op2set = false; //Operation2 is not set
			resultset = false; //Result is not displayed
			clearDisplay = true; //Display is cleared
			model = new CalculatorModel(); //Intialize CalculatorModel for calculations
		}
		
		@Override
		/**
		 * Overloaded actionPerformed for when any button is pressed, use if-else to check for each button press, and calls the required method calls
		 * to achieve the desired button result.
		 * @param e Reference to actionevent that occured
		 */
		public void actionPerformed(java.awt.event.ActionEvent e) {
			String command = e.getActionCommand(); //Assign the actioncommand that was pressed to a string for easier access
			text = display.getText(); //Get the current text of the calculator being displayed
			if(model.getErrorState()) { //First thing to check is if our calculator is currently in an error state, if it is only certain button presses will work
				if(command.equals("C")) { //If user presses clear button
					clear(); //Call our clear method
				}
				else if(command.equals("Int")) { //If user wishes to change the operation mode
					operationMode(); //Call method to change operation mode
				}
				else if(command.equals(".0") || command.equals(".00") || command.equals("Sci")) { //If user wishes to change the precision of the calculator
					model.setPrecision(command); //call method to change calculator's precision
				}
			}
			else if(command.equals("Int")) { //If user wishes to change the operation mode
				operationMode(); 
			}
			else if(command.equals(".0") || command.equals(".00") || command.equals("Sci")) { //If user wishes to change calculator's precision
				model.setPrecision(command);
			}
			else if(command == BACKSPACE) {  //If user wishes to delete a char in text display, backspace pressed
				backspace();
			}
			else if(command == PLUSMINUS) { //If user wishes to change the sign of the number, +- pressed
				sign();
			}
			else if(command.equals(DIVISION) || command.equals(MULTIPLY) || command.equals(PLUS) || command.equals(MINUS)) { //If operator button was pressed
				operator(command);
			}
			else if(command.equals("C")) { //If user wishes to clear the text field, Pressed C
				clear();
			}
			else if (command.equals(".")) { //If user wishes to add a decimal point to the text field, Pressed .
				decimal();
			}
			else if (command.equals("=")) { //If user wishes to calculate the new number, Pressed =
				calculate();
			}
			else { //Else the button they pressed was from 0-9, a number
				if(resultset) { //If a result is currently shown on the calculator, clear it, will be a new calculation
					clear();
				}
				//If the display has been cleared and is currently in default display, or they just have a sign operator (looking to create a negative number)
				if(clearDisplay && display.getText().equals(model.getPrecisionZero()) || display.getText().equals("-")) { 
					if(!display.getText().equals("-")) { //If text doesn't include - , we can just set the text to the number pressed
						display.setText(command);
					}
					else { //Else we must append to the end, keeping the -
						display.setText(display.getText() + command);
					}
					clearDisplay = false; //The display is no longer cleared
					return; //Return out 
				}
				display.setText(display.getText() + command); //Else we just need to append to the end of display
				model.setOperand2(Double.parseDouble(display.getText())); //Set operand2 as the current display value, parsed as a double
			}
		}
		
		/**
		 * Method backspace(), used to handle what occurs when the backspace button is pressed. If all the text is backspaced, it will display the
		 * default number of the current operation mode.
		 */
		private void backspace() {
			if(resultset) { //We are not allowed to backspace if a result is showing
				return;
			}
			if((text.startsWith("-") && text.length() == 2) || text.length() == 1) { //If we happen to backspace all chars, set the the default precision mode zero
				display.setText(model.getPrecisionZero());
			}
			else { //Else substring the last index of the string
				display.setText(text.substring(0, display.getText().length()-1));
			}	
		}
		
		/**
		 * Method decimal(), used to handle what occurs when the . (dot button) is pressed. Only appends if there is currently no other .
		 */
		private void decimal() {
			if(text.isEmpty()) { //If the text is currently empty, make text 0, the decimal cannot be pressed in int mode, so we do not need to check for mode
				display.setText("0");
			}
			if(!text.contains(".")) { //Onlt append a . if there is currently no other decimal present
				display.setText(text + ".");
			}
		}
		/**
		 * Method sign(), used to handle what occurs when the +-(PLUS MINUS) button is pressed. Changes the sign of the text.(Number later on)
		 */
		private void sign() {
			if(text.startsWith("-")) { //If there is currently a sign at the start, we should remove it
				text = text.substring(1); //Substring the at index 1
				display.setText(text); //Set the display to the split string
			}
			else if(text.equals(model.getPrecisionZero())) { //If the current display is 0 or 0.0, delete it and set to -
				display.setText("-");
			}
			else { //Else - was not found, append to the start of the string
				text = "-" + text;
				display.setText(text);
			}
		}
		
		/**
		 * Method calculate(), used to handle what occurs when the =(equals) button is pressed. Sets the operand's and calls the CalculatorModel performCalculations()
		 * and sets the display to the newly calculated result. Is also used to change the precision of the number.
		 */
		private void calculate() {
			if(!op2set) { //If the 2nd operand is not set, we should try to set it.
				double operand2;
				if(!text.isEmpty()) { //If the calculator is displaying something
					operand2 = Double.parseDouble((text)); //Parse the text and remove the E, to succesfully parse as double
				}
				else { //Else they didn't enter a 2nd operand, we should use the first as the second
					operand2 = model.getOperand1();
					model.setOperand2(operand2);
				}
			}
			model.performCalculations(); //Calculate the value
			display.setText(model.getResult()); //Set the result to the calculator's display
			resultset = true; //A result is now displayed
			if(model.getErrorState()) { //If the calculation caused an error, set the error label to error mode
				error.setText("E");
				error.setBackground(Color.RED);
			}
		}
		
		/**
		 * Method clear(), used to handle what occurs when the C (Clear) button is pressed. Used to clear the display and reset all required boolean flags and set
		 * the display to the operation mode's default text
		 */
		private void clear() {
			model.clear(); //Clear the CalculatorModel operand's and operator
			op1set = op2set = resultset = false; //Reset boolean flags
			clearDisplay = true;
			model.setErrorState(false); //Reset the error state
			display.setText(model.getPrecisionZero()); //Set the calculator display to the operation mode default zero
			if(model.getMode().equals("Int")) { // If the calculator's mode is INT, set the required label attributes and disable the . button
				dotButton.setEnabled(false);
				error.setText("I");
				error.setBackground(Color.GREEN);
			}
			else { //Else the calculator is in FLOAT mode
				dotButton.setEnabled(true);
				error.setText("F");
				error.setBackground(Color.YELLOW);
			}
		}
		
		/**
		 * Method operationMode() used to change the calculator's operation mode from Int to float and vice versa.Changes the operation mode label to the 
		 * correct attributes based on the operation mode.
		 */
		private void operationMode() {
			if(model.getMode().equals("Int")) { //If we are currently in an Int mode, we must switch to FLOAT
				dotButton.setEnabled(true); //Enable to . button
				model.setMode("Float"); //Set the mode to FLOAT
				if(!model.getErrorState()) { //If the calculator is not in an error state we may change the labels physical appearance
					error.setText("F");
					error.setBackground(Color.YELLOW);
					if(display.getText().equals("0")) { //If the text is 0(Int default zero), we should change to default float zero
						display.setText(model.getPrecisionZero()); 
					}
				}
			}
			else { //Else it is currently in FLOAT, we must switch to INT
				dotButton.setEnabled(false); //Disable the . button
				model.setMode("Int"); //Set the mode to Int
				if(!model.getErrorState()) { //If the calculator is not in an error state we may change the labels phyiscal apperance
					error.setBackground(Color.GREEN); //Set the label to GREEN
					error.setText("I");
					display.setText(String.valueOf(Float.valueOf(display.getText()).intValue())); //Take the float value and convert to an int value
					if(Float.valueOf(display.getText()) == 0.0){ //If the text is 0.0(Float default zero), we should change to default int zero
						display.setText(model.getPrecisionZero());
					}
				}
			}
		}
		
		/**
		 * Method operator, used when an operator button is pressed(*,/,-,+), checks the various boolean flags and sets the operand and operator if applicable
		 * @param operator - The operation for the calculation that was returned
		 */
		private void operator(String operator) {
			if(resultset) { //If a result is already displayed the the screen 
				model.setOperand1WithResult(); //we should make operand1 equal to the result
				resultset = false; //Result is no longer displayed
				op1set = true; //Operand1 set
				op2set = false; //Operand2 not set
				clearDisplay = false; //Display is no longer cleared
			}
			if(clearDisplay) { //If the calculator has been cleared, reset operand1
				model.setOperand1(0); //Reset operand1 to 0
				op1set = false; //Operand1 no long set
				clearDisplay = false; 
			}
			if(!op1set) { //If operand1 is not set, we should attempt to set it, because they entered an operator
				double operand = Double.parseDouble(display.getText()); //Parse the current text as double
				model.setOperand1(operand); //Set operand1
				op1set = true;
			}
			if(op2set) { //If operand2 is already set, we should perform calculations, they wish to enter another number
				model.performCalculations(); //Perform the calculations
				display.setText(model.getResult());//set the display to the calculated result
				if(model.getErrorState()) { //If we are currently in an error state, we should exit the function
					return;
				}
				model.setOperand1WithResult(); //Set operand1 to the current result that is displayed
				op1set = true;
				op2set = false;
			}
			model.setOperation(operator); //Set the operation for next calculation
			display.setText(""); //Reset the display
		}
	}
}
		
		


	


