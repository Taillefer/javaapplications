/* ******************************************************************************
 * File Name: CalculatorView.java
 * Author: Brodie Taillefer
 * Course: CST 8221- JAP, Lab Section: TBD
 * Assignment: 1
 * Date: February 13, 2015
 * Professor: Svillen Ranev
*  Purpose: Used to create and paint the Calculator and it's GUI. This includes adding the Action method's for 
*  			each of the Calculator's buttons.
 *******************************************************************************/


import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

/**
 * Used to create and paint the Calculator's GUI and button action handler's. The calculator is arranged so that resizing
 * the calculator will not alter the appearance.
 * @author Brodie Taillefer
 * @version V1.0
 * @see java.awt*,javax.awt.event.ActionListener,java.awt.event.KeyEvent,javax.swing.*,javax.swing.border.EmptyBorder
 * @since 1.8_025
 */
public class CalculatorView extends JPanel{
	/** serialVersion of the application {@value #serialVersionUID}*/
	private static final long serialVersionUID = 1L;
	/**textField for the calculator display */
	private JTextField display;
	/** Label for the error */
	private JLabel error;
	/** JButton for the calculator's . button. It will be initialized using our button factory method, createButton() */
	@SuppressWarnings("unused")
	private JButton dotButton;
	/** Default font size of {@value #DEFAULT_FONT_SIZE} */
	private final int DEFAULT_FONT_SIZE = 20;
	/**String array for the button labels  */
	private final static String[] LABELS = new String[] {"7", "8", "9", "\u00F7", "4", "5", "6", "\u002A",
					"1", "2", "3", "\u002D", "\u00B1", "0", ".", "\u002B"};
	/**
	 * Method used to initialize and paint the Calculator GUI, and attach the button's to their Action Listener
	 */
	public CalculatorView(){
		display = new JTextField(); //Initialize textField
		this.setLayout(new BorderLayout()); //Our Main layout for our JPanel will be a borderlayout
		this.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color.BLACK));
		JPanel upperPanel = new JPanel(new BorderLayout()); //JPanel with borderlayout for the upper panel 
		JPanel displayPanel = new JPanel(); //JPanel with default flow layout for the display panel
		JPanel lowerPanel = new JPanel(); //JPanel with default flow layout for the lower panel
		error = new JLabel("F");
		error.setPreferredSize(new Dimension(25,25)); //Set the size for the error label 
		error.setHorizontalAlignment(JLabel.CENTER); //Set the mode text to the center of the label
		error.setOpaque(true); //Set Opaque so we can add a bg colour
		error.setBackground(Color.YELLOW);
		displayPanel.add(error); //Add the mode display to displayPanel

		display.setEditable(false);
		display.setPreferredSize(new Dimension(0,30)); //Set textField height to 30
		display.setColumns(16);
		display.setBackground(Color.WHITE);
		display.setHorizontalAlignment(JTextField.RIGHT);
		display.setText("0.0");
		displayPanel.setBackground(Color.WHITE);//set the display panel bg to white
		displayPanel.add(display); //Add the textField to the displayPanel
		
		JButton backspace = new JButton("\u2190"); //Backspace button for first row
		backspace.setPreferredSize(new Dimension(25,25)); //Set backspace's button pref size
		backspace.setBorder(BorderFactory.createLineBorder(Color.RED,1));
		backspace.setForeground(Color.RED);
		backspace.setMnemonic(KeyEvent.VK_B); //Set mnemonic for backspace ALT + B
		backspace.setToolTipText("BackSpace(ALT + B)");
		backspace.setOpaque(false);
		backspace.setContentAreaFilled(false); //Set's the button transparant
		backspace.addActionListener(new Controller());
		displayPanel.add(backspace);
		
		upperPanel.add(displayPanel,BorderLayout.NORTH); //Add our displaypanel to the north part of our upper panel
		JCheckBox mode = new JCheckBox(); //Checkbox for the Int mode
		mode.setBackground(Color.GREEN);
		mode.addActionListener(new Controller());
		mode.setText("Int");
		mode.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		ButtonGroup precisionGroup = new ButtonGroup(); //Button group to hold the precision Radio Buttons
		JRadioButton zeroPrecision = new JRadioButton(); //0.0 Precision button
		
		zeroPrecision.addActionListener(new Controller());
		zeroPrecision.setText(".0");
		zeroPrecision.setBackground(Color.YELLOW);
		precisionGroup.add(zeroPrecision); //Add the Radio button to the buttongroup
		
		JRadioButton zeroZeroPrecision = new JRadioButton(); //0.00 Precision Button
		zeroZeroPrecision.addActionListener(new Controller());
		zeroZeroPrecision.setText(".00");
		zeroZeroPrecision.setSelected(true);
		zeroZeroPrecision.setBackground(Color.YELLOW);
		precisionGroup.add(zeroZeroPrecision); //Add the Radio button to the buttongroup
		
		JRadioButton sciPrecision = new JRadioButton(); //sci Precision Button
		sciPrecision.addActionListener(new Controller());
		sciPrecision.setText("Sci");
		sciPrecision.setBackground(Color.YELLOW);
		zeroPrecision.setPreferredSize(sciPrecision.getPreferredSize()); //Set the size of the button to the largest radio button's size
		zeroZeroPrecision.setPreferredSize(sciPrecision.getPreferredSize()); 
		mode.setPreferredSize(sciPrecision.getPreferredSize()); 
		precisionGroup.add(sciPrecision); //Add the radio button to the button group
		
		Box modeBox = new Box(BoxLayout.X_AXIS);	//Box Layout in the X Axis to hold the int mode and precision
		modeBox.setBackground(Color.BLACK);
		modeBox.add(Box.createHorizontalStrut(12));//Create horizontal struct to line up to mode with the textfield
		modeBox.add(mode);
		modeBox.add(Box.createHorizontalStrut(19)); //Create horizontal struct for space between mode and Radio Buttons
		modeBox.add(zeroPrecision);modeBox.add(zeroZeroPrecision);modeBox.add(sciPrecision);
		lowerPanel.add(modeBox); //Add Box to the lowerPanel 
		lowerPanel.setBackground(Color.BLACK);
		upperPanel.add(lowerPanel,BorderLayout.CENTER); //Add lowerPanel to the Center of the upperPanel with a centered layout
		this.add(upperPanel,BorderLayout.NORTH); //Add the upperPanel to the north of the main layout
		
		JButton clearButton = createButton("C","C",Color.BLACK,Color.RED,new Controller()); //Create Clear Button
		upperPanel.add(clearButton,BorderLayout.SOUTH);
		
		GridLayout grid = new GridLayout(4,1,5,5); //Create grid layout with the following dimensions
		JPanel calcButtons = new JPanel(grid); //JPanel with grid layout for the calculator's buttons
		calcButtons.setBorder(new EmptyBorder(2,2,2,2));
		
		for(String labels : LABELS) { //Loop through the array of button name's and create the corresponding button
			JButton button; //Create button variable
			if(labels == ".") {
				button = createButton(labels,labels,Color.BLACK,Color.BLUE,new Controller());
				this.dotButton = button;
			}
			if(!labels.matches("[\u00F7\\\u002A\\\u002D\\\u002B]")){ //If the label doesn't match the regex, the buttons fg should be black
				button = createButton(labels,labels,Color.BLACK,Color.BLUE,new Controller());
			}
			else { //Else it should be Yellow
				button = createButton(labels,labels,Color.YELLOW,Color.BLUE,new Controller());
			}
			calcButtons.add(button); //Add the button to the JPanel with GridLayout
		}
		JButton equals = createButton("\u003D", "\u003D", Color.BLACK, Color.YELLOW, new Controller()); //Create equals Button
		equals.setFont(equals.getFont().deriveFont(Font.BOLD));
		equals.setPreferredSize(clearButton.getPreferredSize()); //Set it's size to the same as Clear Button
		this.add(calcButtons,BorderLayout.CENTER); //Set the grid layout of buttons in the main Panels Center
		this.add(equals,BorderLayout.SOUTH); //Set the equals button to south in the main panel
		this.setVisible(true); //Show GUI
	}
	
	/** Method used to create a button with the given properties. This is used to create all the buttons situated
	 * in the Calcullator's grid layout. The button is also given a ActionListener to keep track of when the user
	 * pressed and releases the button.
	 * @param text The text for the button label 
	 * @param ac The Action command string for the button label
	 * @param fg The foreground color of the button
	 * @param bg The background color of the button
	 * @param handler The Action Handler for the button
	 * @return JButton Return reference to the button
	 */
	private JButton createButton(String text, String ac, Color fg, Color bg, ActionListener handler){
		JButton button = new JButton(); //Create button with no properties set
		button.setText(text);
		button.setBackground(bg);
		button.setForeground(fg);
		if(ac != null) { //If a valid action command string has been passed in
			button.setActionCommand(ac);
		}
		Font defaultFont = UIManager.getDefaults().getFont("TabbedPane.font"); //Get the default font that is currently be used
		button.setFont(new Font(defaultFont.getFontName(),defaultFont.getStyle(),DEFAULT_FONT_SIZE)); //Set Font to the default font being used and 20 font size
		button.addActionListener(handler);	//Add the ActionListener to the button
		return button;	
	}

	/**
	 * Inner class to create an ActionListener for the Calculator's buttons. The ActionListener will append
	 * the button pressed to the display field.
	 * @author Brodie Taillefer
	 * @version V1.0
	 * @see java.awt,javax.swing
	 * @since 1.8_025
	 */
	 private class Controller implements ActionListener {
		@Override
		/**
		 * Overloaded actionPerformed for when the button is pressed, it will append the button's text to the Calculator's
		 * display screen.
		 * @param e
		 */
		public void actionPerformed(java.awt.event.ActionEvent e) {
			display.setText(display.getText() + e.getActionCommand());	//Concat the current tex tin the text field with the button's action command	
		}
	 }
}
	


