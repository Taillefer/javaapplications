/* ******************************************************************************
 * File Name: CalculatorSplashScreen.java
 * Author: Brodie Taillefer
 * Course: CST 8221- JAP, Lab Section: TBD
 * Assignment: 1
 * Date: February 13, 2015
 * Professor: Svillen Ranev
*  Purpose: Used to display a splash screen before the main calculator GUI is shown
*           this class includes the method showSplashWindow() to paint the splash screen
*           for 5 seconds before the main GUI shows.
 *******************************************************************************/


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Used to paint and display the Calculator's SplashScreen for x seconds
 * @author Brodie Taillefer
 * @version V1.0
 * @see java.awt.BorderLayout,java.awt.Color,java.awt.Font,javax.swing.BorderFactory,javax.swing.ImageIcon, javax.swing.JFrame, javax.swing.JLabel, 
 * 			 javax.swing.JPanel
 * @since 1.8_025
 */

public class CalculatorSplashScreen  {
	/** Duration for the splash screen to show */
	private int duration;
	
	/**
	 * Constructor for the CalculatorSplashScreen class. Takes the duration of the splashscreen to show
	 * as a parameter.
	 * @param duration Duration for the splashscreen to show
	 */
	public CalculatorSplashScreen(int duration) {
	    this.duration = duration;
	 }
	
	/**
	 * Method used to initialize and paint the Splash window to the screen before the main calculator GUI
	 * is shown. The splash screen is centered in the screen and includes the author's student number and name.
	 */
	public void showSplashWindow(){
		JFrame frame = new JFrame(); //Frame for the splash window
		frame.setUndecorated(true);
		JPanel splash = new JPanel(new BorderLayout()); //JPanel to add components for the splash screen	
	  frame.setLocationByPlatform(true); //Allow the operating system to chose the window location
		JLabel label = new JLabel(new ImageIcon("calculator.jpg")); //New Image for the splash screen
	  JLabel demo = new JLabel("Brodie Taillefer\n 040 757 711", JLabel.CENTER); //Label for the splash screen
	  demo.setFont(new Font(Font.SERIF, Font.BOLD, 25));
	  demo.setOpaque(true);
	  demo.setBackground(Color.BLACK); //Set the label's bg color to Black
	  demo.setForeground(Color.WHITE); //Font color white
	  splash.setBorder(BorderFactory.createLineBorder(Color.BLACK, 9));
	  splash.add(label, BorderLayout.CENTER);
	  splash.add(demo, BorderLayout.SOUTH);
    frame.add(splash); //Add the splash to the main frame
	  frame.setContentPane(splash); //Set the splash screen in the content frame's content pane
	  frame.pack();
	  frame.setVisible(true);
	  try {	//Sleep for the duration of the splash screen
	  	Thread.sleep(duration); }
	  catch (Exception e) {e.printStackTrace();}
	  //destroy the window and release all resources
	  frame.dispose(); //Release the frame's resources
	}
}
